FROM pull-through.cloud.duke.edu/library/python:3

RUN pip install speedtest-cli

ENTRYPOINT ["/usr/local/bin/speedtest-cli"]